#include <stdlib.h>
#include <unistd.h>

typedef struct Header {
  unsigned int size;
  struct Header *next;
} blockheader;

static blockheader baseb;           /* Zero sized block header */
static blockheader *freebp = &baseb; /* First free block pointer */
static blockheader *usedbp;         /* First used block pointer */

/* Scan free list and look for a place for the block */

static void
add_block_to_free_list(blockheader *bp) {
  blockheader *p;

  for (p = freebp; !(bp > p && bp < p->next); p = p->next)
    if (p >= p->next && (bp > p || bp < p->next))
	break;
  
  if (bp + bp->size == p->next) {
    bp->size += p->next->size;
    bp->next = p->next->next;
  } else { bp->next = p->next; }
  if (p + p->size == bp) {
    p->size += bp->size;
    p->next = bp->next;
  } else { p->next = bp; }

  freebp = p;
}

/* request memory */

static blockheader *
reqmem(size_t num_units) {
  void *vbp;
  blockheader *up;

  if (num_units > 4096)
    num_units = 4096 / sizeof(blockheader);
  vbp = sbrk(num_units * sizeof(blockheader));
  if (vbp == (void *) -1) return NULL;

  up = (blockheader *) vbp;
  up->size = num_units;
  add_block_to_free_list(up);
  return freebp;
}
