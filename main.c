#include <stdlib.h>
#include <unistd.h>
#include "helper.h"


/*
 * Find chunks from the free list and move them to the used list
 * Also asks for more if there's not enough memory
 */

void *
mem_allocate_gc(size_t allocation_size) {
  size_t num_units;
  blockheader *bp, *prevbp;

  num_units = (allocation_size + sizeof(blockheader) - 1) / sizeof(blockheader) + 1;
  prevbp = freebp;

  for (bp = prevbp->next;; prevbp = bp, bp = bp->next) {
    if (bp->size >= num_units) { 
      /* if big enough */
      if (bp->size == num_units) {
	/* if exact size */
	prevbp->next = bp->next;
      } else {
	bp->size -= num_units;
	bp += bp->size;
	bp->size = num_units; 
      }

      freebp = prevbp;


      /* add p to used list */
      if (usedbp == NULL) {
	usedbp = bp->next = bp;
      } else {
	bp->next = usedbp->next;
	usedbp->next = bp;
      }

      return (void *) (bp + 1);
    }
    if(bp == freebp) {
      /* Not enough memory */
      bp = reqmem(num_units);
      /* if memory request fails */
      if (bp == NULL) return NULL;
    }
  }
}
